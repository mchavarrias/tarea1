/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Objetos.objEquipos;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author manue
 */
public class Dato {
    
    public void InsertarEnArchivo1(ArrayList<objEquipos> listaEquipos) {
        try {
            File archivo = new File("ListaEquipos.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, false));
            String datos = "";
            for (int i = 0; i < listaEquipos.size(); i++) {
                datos = "";
                String codigoEquipo = listaEquipos.get(i).getCodigoEquipo();
                String nombreEquipo = listaEquipos.get(i).getNombreEquipo();
                String ganador = listaEquipos.get(i).getGanador();
                String empate = listaEquipos.get(i).getEmpate();
                String perdido = listaEquipos.get(i).getPerder(); 
                datos = codigoEquipo + "," + nombreEquipo + "," + ganador + "," + empate + "," + perdido;
                archi.write(datos + "\r\n");
            }
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al escribir en el archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static ArrayList<objEquipos> LeerDesdeArchivoObjeto() {
        try {
            File archivo = new File("ListaEquipos.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            objEquipos.listaEquipos = new ArrayList();
            while (archi.ready()) {
                String[] separar = new String[5];
                separar = archi.readLine().split(",");
                objEquipos.listaEquipos.add(new objEquipos(separar[0], separar[1], separar[2], separar[3], separar[4]));
            }
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer del archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        return objEquipos.listaEquipos;
    }
    
    public void InsertarEnArchivo (String datosEquipos){
        try{
            File archivo = new File("ListaEquipos.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(datosEquipos + "\r\n");
            archi.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error al escribir en el archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
}
