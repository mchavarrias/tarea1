/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import Datos.*;
import java.util.ArrayList;
import Objetos.*;

/**
 *
 * @author manue
 */
public class Equipos {
    
    Dato datoEquipos = new Dato();

    public void InsertarUsuario(ArrayList<objEquipos> listaEquipos) {
        String dato = "";
        for (int i = 0; i <= listaEquipos.size() - 1; i++) {
                String codigoEquipo = listaEquipos.get(i).getCodigoEquipo();
                String nombreEquipo = listaEquipos.get(i).getNombreEquipo();
                String ganador = listaEquipos.get(i).getGanador();
                String empate = listaEquipos.get(i).getEmpate();
                String perdido = listaEquipos.get(i).getPerder(); 
                dato = codigoEquipo + "," + nombreEquipo + "," + ganador + "," + empate + "," + perdido;
        }
        datoEquipos.InsertarEnArchivo(dato);
    }

    public ArrayList<objEquipos> LeerUsuariosObjetos() {
        ArrayList<objEquipos> listaEquipos = new ArrayList<objEquipos>();
        listaEquipos = datoEquipos.LeerDesdeArchivoObjeto();
        return listaEquipos;
    }
    
    public void guardar(ArrayList<objEquipos> listaEquipos){
        datoEquipos.InsertarEnArchivo1(listaEquipos);
    }
}