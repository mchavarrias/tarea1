/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author manue
 */
public class objEquipos {
    
    private String codigoEquipo;
    private String nombreEquipo;
    private String ganador;
    private String perder;
    private String empate;
    private int marcador;

    public objEquipos(String codigoEquipo, String nombreEquipo, String ganador, String empate, String perder) {
        this.nombreEquipo = nombreEquipo;
        this.ganador = ganador;
        this.perder = perder;
        this.empate = empate;
        this.codigoEquipo = codigoEquipo;
    }

    public static ArrayList listaEquipos = new ArrayList<>();
    
    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public String getGanador() {
        return ganador;
    }

    public void setGanador(String ganador) {
        this.ganador = ganador;
    }

    public String getPerder() {
        return perder;
    }

    public void setPerder(String perder) {
        this.perder = perder;
    }

    public String getEmpate() {
        return empate;
    }

    public void setEmpate(String empate) {
        this.empate = empate;
    }

    public int getMarcador() {
        return marcador;
    }

    public void setMarcador(int marcador) {
        this.marcador = marcador;
    }

    public String getCodigoEquipo() {
        return codigoEquipo;
    }

    public void setCodigoEquipo(String codigoEquipo) {
        this.codigoEquipo = codigoEquipo;
    }
}
